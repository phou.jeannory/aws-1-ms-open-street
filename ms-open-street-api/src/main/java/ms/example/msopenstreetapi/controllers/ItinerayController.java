package ms.example.msopenstreetapi.controllers;

import lombok.RequiredArgsConstructor;
import ms.example.msopenstreetapi.models.OpenStreetTravelTime;
import ms.example.msopenstreetapi.services.OpenStreetApiService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/itineray")
public class ItinerayController {

    private final OpenStreetApiService openStreetApiService;

    /**
     *
     * @param from lat,long
     * @param to lat,long
     * @param mode driving
     * exemple http://localhost:8081/api/itineray/calculate?from=46.930455,-0.254480&to=47.374872,3.960785&mode=driving
     */
    @GetMapping("/calculate")
    public ResponseEntity<OpenStreetTravelTime> calculateTravelTime(
            @RequestParam String from,
            @RequestParam String to,
            @RequestParam String mode
    ) {
        return ResponseEntity.ok(openStreetApiService.calculateItinerary(from, to, mode));
    }

}
