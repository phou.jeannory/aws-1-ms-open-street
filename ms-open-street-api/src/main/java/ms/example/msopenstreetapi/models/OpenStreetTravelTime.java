package ms.example.msopenstreetapi.models;

import lombok.*;
import lombok.extern.jackson.Jacksonized;

import java.util.List;
@Value
@Builder(toBuilder = true)
@Jacksonized
public class OpenStreetTravelTime {
    Straight straight;
    String status;
    List<Road> road;
    Stats stats;

    @Value
    @Builder(toBuilder = true)
    @Jacksonized
    public static class Road {
        String polyline;
        double distance;
        double duration;
    }

    @Value
    @Builder(toBuilder = true)
    @Jacksonized
    public static class Stats {
        double time;
    }

    @Value
    @Builder(toBuilder = true)
    @Jacksonized
    public static class Straight {
        double distance_spheric;
        double distance_vincenty;
        double distance_haversine;
        double duration_guessed;
        String polyline;
    }

}


