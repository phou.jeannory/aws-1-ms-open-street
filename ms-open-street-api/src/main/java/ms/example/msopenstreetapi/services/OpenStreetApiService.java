package ms.example.msopenstreetapi.services;

import ms.example.msopenstreetapi.models.OpenStreetTravelTime;

public interface OpenStreetApiService {

    OpenStreetTravelTime calculateItinerary(String from, String to, String mode);
}
