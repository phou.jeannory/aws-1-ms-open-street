package ms.example.msopenstreetapi.services.impl;

import lombok.RequiredArgsConstructor;
import ms.example.msopenstreetapi.models.OpenStreetTravelTime;
import ms.example.msopenstreetapi.services.OpenStreetApiService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class OpenStreetApiImpl implements OpenStreetApiService {

    @Value("${app.open-street-service.base-url}")
    private String baseUrl;

    @Value("${app.open-street-service.api-key}")
    private String apiKey;

    private final RestTemplate restTemplate;

    /**
     *
     * @param from lat,long
     * @param to lat,long
     * @param mode driving
     */
    @Override
    public OpenStreetTravelTime calculateItinerary(String from, String to, String mode) {
        return restTemplate.getForObject(baseUrl + "/route/?origin=" + from +
                "&destination=" + to + "&mode=" + mode + "&key=" + apiKey, OpenStreetTravelTime.class);
    }


}
