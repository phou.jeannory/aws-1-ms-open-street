{{/* templates/_helpers.tpl */}}
{{- define "open-street-api-chart.fullname" -}}
{{- printf "%s" .Release.Name -}}
{{- end -}}

{{- define "open-street-api-chart.name" -}}
{{- printf "%s" .Chart.Name -}}
{{- end -}}

{{/* Définition du modèle server-port */}}
{{- define "server-port" -}}
{{ .Values.server.port }}
{{- end -}}
