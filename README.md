# ms-open-street-api



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Run app on local environment

```
cd ms-open-street-api
docker build -t ms-open-street-api:latest -f DockerfileLocal .
docker images
docker run -p 8081:8081 ms-open-street-api:latest
```

## Run with minikube method 1 from local project

```
cd ms-open-street-api
docker build -t ms-open-street-api:latest -f Dockerfile .
cd ../ms-open-street-api-chart
docker images
minikube start
kubectl create namespace dev-namespace
minikube image load ms-open-street-api:latest
# on lens set manualy variables on config/configmaps and restart the deployment
helm install open-street-api-chart . --namespace dev-namespace --values values-dev.yaml
# on lens set manualy port 8081 on worklads/pods/ports/forward
```

## Run with minikube method 2 from container registry

```
cd ms-open-street-api-chart
docker pull registry.gitlab.com/phou.jeannory/aws-1-ms-open-street:latest
docker images
minikube start
kubectl create namespace dev-namespace
minikube image load registry.gitlab.com/phou.jeannory/aws-1-ms-open-street:latest
# on lens set manualy variables on config/configmaps and restart the deployment
helm install open-street-api-chart . --namespace dev-namespace --values values-dev.yaml
# on lens set manualy port 8081 on worklads/pods/ports/forward
```

## Run with minikube method 3 from container registry

```
cd ms-open-street-api-chart
minikube start
kubectl create namespace dev-namespace
helm install open-street-api-chart . --set image.repository=registry.gitlab.com/phou.jeannory/aws-1-ms-open-street,image.tag=latest --namespace dev-namespace --values values-dev.yaml
# on lens set manualy variables on config/configmaps and restart the deployment
minikube service --url open-street-api-container-service -n dev-namespace
```

## Upgrade after a newer version (erase all configmap values)

```
cd ms-open-street-api-chart
helm upgrade open-street-api-chart . --set image.repository=registry.gitlab.com/phou.jeannory/aws-1-ms-open-street,image.tag=latest --reuse-values --namespace dev-namespace --values values-dev.yaml

```

## Update after a newer version (require a manual restart)

```
cd ms-open-street-api-chart
kubectl set image deployment/open-street-api-chart open-street-api-chart=registry.gitlab.com/phou.jeannory/aws-1-ms-open-street:latest --namespace=dev-namespace

```